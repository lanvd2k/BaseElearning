# Guide
## Technology
- Net core api 7
- MongoDB
## Require
- Project requires [.Net SDK 7.0](https://dotnet.microsoft.com/en-us/download/dotnet/7.0) to run.
- Database uses [MongoDB](https://www.mongodb.com/)
- IDE [Visual Studio](https://visualstudio.microsoft.com/vs/#download)
## Installation
### Configure MongoDB
- Open `mongosh`
- Run the following command in the command shell `use Elearning`
- Create a `Courses` collection using following command `db.createCollection('Courses')`
### Create Create the ASP.NET Core web API project
- Go to **File > New > Project**.
- Select the **ASP.NET Core Web API** project type, and select Next.
- Name the project *BaseElearning*, and select **Next**.
- Select the **.NET 7.0 (Standard Term Support)** framework and select Create.
- From the *Tools* menu, select *NuGet Package Manager > Package Manager Console*.
- In the **Package Manager Console** window, navigate to the project root. Run the following command to install the .NET driver for MongoDB: `Install-Package MongoDB.Driver`
### Run the project