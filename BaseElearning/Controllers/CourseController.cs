﻿using BaseElearning.Models;
using BaseElearning.Services.IService;
using Microsoft.AspNetCore.Mvc;

namespace BaseElearning.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly ICourseService _courseService;

        public CourseController(ICourseService courseService)
        {
            _courseService = courseService;
        }

        [HttpGet]
        public async Task<List<Course>> GetAll()
        {
            return await _courseService.GetAllCourseAsync();
        }

        [HttpGet("{id:length(24)}")]
        public async Task<ActionResult<Course>> Get(string id)
        {
            var course = await _courseService.GetCourseAsync(id);

            if (course is null)
            {
                return NotFound();
            }

            return Ok(course);
        }

        [HttpPost]
        public async Task<IActionResult> Post(Course course)
        {
            await _courseService.CreateCourseAsync(course);

            return CreatedAtAction(nameof(Get), new { id = course.Id }, course);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update(string id, Course updateCourse)
        {
            var course = await _courseService.GetCourseAsync(id);

            if (course is null)
            {
                return NotFound();
            }

            updateCourse.Id        = course.Id;
            updateCourse.UpdatedAt = DateTime.Now;

            var newCourse = await _courseService.UpdateCourseAsync(id, updateCourse);

            return Ok(newCourse);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete(string id)
        {
            var course = await _courseService.GetCourseAsync(id);

            if (course is null)
            {
                return NotFound();
            }

            await _courseService.RemoveCourseAsync(id);

            return NoContent();
        }
    }
}
