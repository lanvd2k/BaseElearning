﻿using BaseElearning.Models;
using BaseElearning.Models.Context.IContext;
using BaseElearning.Repository.IRepository;

namespace BaseElearning.Repository
{
    public class CourseRepository : Repository<Course>, ICourseRepository
    {
        public CourseRepository(IMongoContext context) : base(context)
        {
            
        }
    }
}
