﻿using BaseElearning.Models.Context.IContext;
using BaseElearning.Repository.IRepository;
using MongoDB.Driver;

namespace BaseElearning.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<T> _dbSet;
        public Repository(IMongoContext context)
        {
            _database = context.Database;
            _dbSet = _database.GetCollection<T>(typeof(T).Name);
        }
        public async Task CreateAsync(T entity)
        {
            await _dbSet.InsertOneAsync(entity);
        }

        public async Task<T> UpdateAsync(string id, T entity)
        {
            await _dbSet.ReplaceOneAsync(FilterId(id), entity);
            return entity;
        }

        public async Task<List<T>> GetAllAsync()
        {
            var all = await _dbSet.FindAsync(Builders<T>.Filter.Empty);

            return all.ToList();
        }

        public async Task<T> GetAsync(string id)
        {
            var data = await _dbSet.Find(FilterId(id)).SingleOrDefaultAsync();

            return data;
        }

        public async Task RemoveAsync(string id)
        {
            await _dbSet.DeleteOneAsync(FilterId(id));
        }

        private static FilterDefinition<T> FilterId(string key)
        {
            return Builders<T>.Filter.Eq("Id", key);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
