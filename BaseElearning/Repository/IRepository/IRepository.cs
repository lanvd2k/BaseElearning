﻿using System.Linq.Expressions;

namespace BaseElearning.Repository.IRepository
{
    public interface IRepository<T> : IDisposable where T : class
    {
        Task<List<T>> GetAllAsync();
        Task<T> GetAsync(string id);
        Task CreateAsync(T entity);
        Task<T> UpdateAsync(string id, T entity);
        Task RemoveAsync(string id);
    }
}
