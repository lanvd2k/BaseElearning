﻿using BaseElearning.Models;

namespace BaseElearning.Repository.IRepository
{
    public interface ICourseRepository : IRepository<Course>
    {
    }
}
