﻿using BaseElearning.Models.DatabaseSettings.IDatabaseSettings;

namespace BaseElearning.Models.DatabaseSettings
{
    public class DatabaseSetting : IDatabaseSetting
    {
        public string ConnectionString { get; set; } = null!;
        public string DatabaseName { get; set; } = null!;
        public string CollectionName { get; set; } = null!;
    }
}
