﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BaseElearning.Models
{
    public class Course : BaseEntity
    {
        [BsonElement("Name")]
        public string CourseName { get; set; } = null!;

        public decimal Price { get; set; }

        public string Category { get; set; } = null!;

        public string Author { get; set; } = null!;
    }
}
