﻿using BaseElearning.Models.Context.IContext;
using BaseElearning.Models.DatabaseSettings.IDatabaseSettings;
using MongoDB.Driver;

namespace BaseElearning.Models.Context
{
    public class MongoContext : IMongoContext
    {
        public MongoContext(IDatabaseSetting connectionSetting)
        {
            var client = new MongoClient(connectionSetting.ConnectionString);
            Database = client.GetDatabase(connectionSetting.DatabaseName);
        }
        public IMongoDatabase Database { get; }
    }
}
