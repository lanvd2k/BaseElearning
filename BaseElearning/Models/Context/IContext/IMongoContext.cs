﻿using MongoDB.Driver;

namespace BaseElearning.Models.Context.IContext
{
    public interface IMongoContext
    {
        IMongoDatabase Database { get; }
    }
}
