﻿using BaseElearning.Models;

namespace BaseElearning.Services.IService
{
    public interface ICourseService
    {
        Task<List<Course>> GetAllCourseAsync();
        Task<Course> GetCourseAsync(string id);
        Task CreateCourseAsync(Course course);
        Task<Course> UpdateCourseAsync(string id, Course course);
        Task RemoveCourseAsync(string id);
    }
}
