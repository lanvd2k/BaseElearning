﻿using BaseElearning.Models;
using BaseElearning.Repository.IRepository;
using BaseElearning.Services.IService;

namespace BaseElearning.Services
{
    public class CourseService : ICourseService
    {
        private readonly ICourseRepository _courseRepository;
        public CourseService(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }
        public async Task CreateCourseAsync(Course course)
        {
            await _courseRepository.CreateAsync(course);
        }

        public async Task<List<Course>> GetAllCourseAsync()
        {
            return await _courseRepository.GetAllAsync();
        }

        public async Task<Course> GetCourseAsync(string id)
        {
            return await _courseRepository.GetAsync(id);
        }

        public async Task RemoveCourseAsync(string id)
        {
            await _courseRepository.RemoveAsync(id);
        }

        public async Task<Course> UpdateCourseAsync(string id, Course course)
        {
            return await _courseRepository.UpdateAsync(id, course);
        }
    }
}
