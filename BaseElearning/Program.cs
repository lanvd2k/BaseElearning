using BaseElearning.Models.Context;
using BaseElearning.Models.Context.IContext;
using BaseElearning.Models.DatabaseSettings;
using BaseElearning.Models.DatabaseSettings.IDatabaseSettings;
using BaseElearning.Repository;
using BaseElearning.Repository.IRepository;
using BaseElearning.Services;
using BaseElearning.Services.IService;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.Configure<DatabaseSetting>(
    builder.Configuration.GetSection("BaseElearningDatabase"));

builder.Services.AddSingleton<IDatabaseSetting>(options =>
                options.GetRequiredService<IOptions<DatabaseSetting>>().Value);

builder.Services.AddScoped<IMongoContext, MongoContext>();
builder.Services.AddScoped<ICourseRepository, CourseRepository>();
builder.Services.AddScoped<ICourseService, CourseService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
